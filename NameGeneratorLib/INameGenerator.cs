﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameGeneratorLib
{
    public interface INameGenerator
    {
        string getName(int l);
    }
}
