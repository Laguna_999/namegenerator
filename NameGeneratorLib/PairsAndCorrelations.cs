﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameGeneratorLib
{
    public class PairsAndCorrelations : INameGenerator 
    {
        private Dictionary<int, Dictionary<string, float>> Pairs;
        private Dictionary<int, Dictionary<string, float>> Correlations;

        private System.Collections.Generic.Dictionary<string, float> charFrequencies;

        private string allChars;

        private string _collection;
        public string Collection
        {
            get { return _collection; }
            set { _collection = value;  }
        }

        public void SaveFiles()
        {
            SaveCorrelations();
            SavePairs();
        }

        public bool useTupels = true;
        public bool useCorrelations = true;

        private float mean = 0.0f;

        public void LoadFiles(string collection = "default", string trainingFile = "")
        {
            Collection = collection;
            
            LoadCorrelations();
            LoadPairs();
            if (!System.IO.Directory.Exists("../../../data/" + Collection))
            {
                System.IO.Directory.CreateDirectory("../../../data/" + Collection);
                if (!String.IsNullOrEmpty(trainingFile))
                {
                    AnalyzeFile(trainingFile);
                }
                System.IO.File.Copy("../../../data/default/charfrequency.json", "../../../data/" + Collection + "/charfrequency.json");
            }
            LoadCharFrequencies();

            calcMean();
        }

        private void calcMean()
        {
            mean = 0;
            if (useTupels)
            {
                foreach (var kvp in Pairs)
                {
                    foreach (var kvp2 in kvp.Value)
                    {
                        mean += kvp2.Value;
                    }
                }
            }
            if (useCorrelations)
            {
                foreach(var kvp in Correlations)
                {
                    foreach (var kvp2 in kvp.Value)
                    {
                        mean += kvp2.Value;
                    }
                }
            }

        }

        public PairsAndCorrelations(string collection = "default", string trainingFile = "")
        {
            Pairs = new Dictionary<int, Dictionary<string, float>>();
            Correlations = new Dictionary<int, Dictionary<string, float>>();
            charFrequencies = new Dictionary<string, float>();

            LoadFiles(collection, trainingFile);
            BuildAllChars();
            CheckFrequencySum();
        }


        private void AnalyzeFile(string trainingFile)
        {
            string[] list = System.IO.File.ReadAllLines(trainingFile);

            foreach(string s in list)
            {
                SetRating(s, 3.0f);
            }


        }

        private void BuildAllChars()
        {
            allChars = string.Empty;

            foreach (var kvp in charFrequencies)
            {
                allChars += kvp.Key;
            }
        }

        private void LoadCharFrequencies()
        {
            string var = System.IO.File.ReadAllText("../../../data/"+ Collection + "/charfrequency.json");
            charFrequencies = JsonConvert.DeserializeObject < Dictionary<string, float> >(var);
        }


        private void LoadPairs()
        {
            Pairs.Clear();
            if (System.IO.File.Exists("../../../data/" + Collection + "/pairs.json"))
            {

                string tp = System.IO.File.ReadAllText("../../../data/" + Collection + "/pairs.json");
                Pairs = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<string, float>>>(tp);
            }
            else
            {
                for (int i = 2; i != 5; i++)
                {
                    Pairs.Add(i, new Dictionary<string, float>());
                }
            }
        }

        private void SavePairs()
        {
                string tp = JsonConvert.SerializeObject(Pairs, Formatting.Indented);
                System.IO.File.WriteAllText("../../../data/" + Collection + "/pairs.json", tp);
        }


        private void LoadCorrelations()
        {
            Correlations.Clear();
            if (System.IO.File.Exists("../../../data/" + Collection + "/correlations.json"))
            {

                string tp = System.IO.File.ReadAllText("../../../data/" + Collection + "/correlations.json");
                Correlations = JsonConvert.DeserializeObject<Dictionary<int, Dictionary<string, float>>>(tp);
            }
            else
            {
                for (int i = 1; i != 4; i++)
                {
                    Correlations.Add(i, new Dictionary<string, float>());
                }
            }
        }

        private void SaveCorrelations()
        {
            string tp = JsonConvert.SerializeObject(Correlations, Formatting.Indented);
            System.IO.File.WriteAllText("../../../data/" + Collection + "/correlations.json", tp);
        }

        private void CheckFrequencySum ()
        {
            float sum = 0;
            foreach(var kvp in charFrequencies)
            {
                sum += kvp.Value;
            }
            Console.WriteLine("Char Frequency Sum is " + sum);
        }

        // completely randomize all letters
        public string getCompleteRandomName (int length)
        {
            var random = RandomLib.RandomGenerator.Random;
            return new string(Enumerable.Repeat(allChars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        // get a random name with respect to the frequencies defined in charFrequencies
        public string getRanomNameWithFrequencies ( int length)
        {
            string retVal = string.Empty;

            for (int i = 0; i != length; ++i)
            {
                string letter = GetRandomLetterWithFrequencies();
                retVal += letter;
            }

            return retVal;
        }

        

        public float lastOptimizationGain;
        public string PermuteName(string str)
        {
            string retval = str;
            int loopCount = 0;
            lastOptimizationGain = 0;

            while (getValue(retval) < mean)
            {
                loopCount++;
                if (loopCount > 100)
                {
                    break;
                }

                string input = retval;
                float inputValue = getValue(input);

               

                string toReplace = string.Empty;

                int depth = RandomLib.RandomGenerator.Random.Next(2, 5);
                var l = PairsAndCorrelations.getNPairs(retval, depth);

                if (l.Count == 0)
                {
                    continue;
                }

                string minimumpair = string.Empty;
                // match pairs of this word and the Pairs
                // find a random element to replace

                toReplace = l.ElementAt(RandomLib.RandomGenerator.Random.Next(0, l.Count));

                if (string.IsNullOrEmpty(toReplace))
                {
                    continue;
                }
                // get a random other string which has the same length
                string replaceString = Pairs[depth].ElementAt(RandomLib.RandomGenerator.Random.Next(0, Pairs[depth].Count)).Key;
                  
                retval = retval.Replace(toReplace, replaceString);
                float retvalValue = getValue(retval);
                if (retvalValue < inputValue)
                {
                    retval = input; // undo the swap
                }
                else
                {
                    lastOptimizationGain += retvalValue - inputValue;
                }
            }

            return retval;
        }

        private string GetRandomLetterWithFrequencies()
        {
            float v = (float)(RandomLib.RandomGenerator.Random.NextDouble() * 100);
            float sum = 0;
            foreach (var kvp in charFrequencies)
            {
                sum += kvp.Value;
                if (v <= sum)
                {
                    return kvp.Key;
                }
            }
            return string.Empty;
        }

        public float getValue(string str)
        {
            float value = 0;
            float valueTuples = 0;
            if (useTupels)
            { 
                foreach (var kvp1 in Pairs)
                {
                    foreach (var kvp in kvp1.Value)
                    {
                        if (str.Contains(kvp.Key))
                        {
                            valueTuples += kvp.Value;
                        }
                    }
                }
            }
            float valueCorrelations = 0;
            if (useCorrelations)
            {
                foreach (var kvp1 in Correlations)
                {
                    int depth = kvp1.Key;
                    foreach (var kvp in kvp1.Value)
                    {
                        char ch1 = kvp.Key[0];
                        char ch2 = kvp.Key[1];

                        valueCorrelations += GetCorrelationOccurence(str, ch1, ch2, depth) * kvp.Value;
                    }
                }
            }
            value = valueCorrelations + valueTuples;
            return value;
        }

        private int GetCorrelationOccurence(string str, char ch1, char ch2, int depth)
        {
            int occurence = 0;
            if (str.Contains(ch1) && str.Contains(ch2))
            {
                var foundIndexes1 = new List<int>();
                for (int i = str.IndexOf(ch1); i > -1; i = str.IndexOf(ch1, i + 1))
                {
                    // for loop end when i=-1 ('a' not found)
                    foundIndexes1.Add(i);
                }
                var foundIndexes2 = new List<int>();
                for (int i = str.IndexOf(ch1); i > -1; i = str.IndexOf(ch1, i + 1))
                {
                    // for loop end when i=-1 ('a' not found)
                    foundIndexes2.Add(i);
                }

                foreach (var lp1 in foundIndexes1)
                {
                    foreach (var lp2 in foundIndexes2)
                    {
                        if (lp2 - lp1 == depth+1)
                        {
                            occurence += 1;
                        }
                    }
                }
            }
            return occurence;
            
        }

        public string getName(int length)
        {
            string retval = String.Empty;
           // calcMean();
            bool success = false;
            while (!success)
            {
                retval = PermuteName(getRanomNameWithFrequencies(length));
               // System.Console.WriteLine(retval + " " + getValue(retval));

                if (getValue(retval)/length > 2000)
                {
                    success = true;
                }
            }
            return retval;
        }


        public static string SanitizeString(string str)
        {
            string retval = string.Empty;
            string temp = str.Trim();
            
            foreach (char c in temp)
            {
                retval += char.ToLower(c);
            }
            return retval;
        }

        public void SetRating(string str, float val)
        {
            str = SanitizeString(str);

            foreach (var kvp1 in Pairs)
            {
                int depth = kvp1.Key;
                var thisPair = kvp1.Value;

                var l = PairsAndCorrelations.getNPairs(str, depth);
                foreach (var v in l)
                {
                    float o;
                    float factor = (float)(depth) * (float)(depth) * 1.25f;
                 
                    if (thisPair.TryGetValue(v, out o))
                    {
                        thisPair[v] += val * factor;
                    }
                    else
                    {
                        thisPair.Add(v, val * factor);
                    }
                }
            }


            foreach (var kvp1 in Correlations)
            {
                int depth = kvp1.Key;
                var thisCorrelation = kvp1.Value;

                var l = PairsAndCorrelations.getNCorrelations(str, depth);

                foreach (var v in l)
                {
                    float o;
                    if (thisCorrelation.TryGetValue(v, out o))
                    {
                        thisCorrelation[v] += val;
                    }
                    else
                    {
                        thisCorrelation.Add(v, val);
                    }
                }
            }
        }







        public static void capitalize (ref string str )
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = char.ToUpper(str[0]) + str.Substring(1);
            }
        }

        public static System.Collections.Generic.List<string> getNPairs(string str, int n)
        {
            System.Collections.Generic.List<string> retval = new List<string>();
            if (string.IsNullOrEmpty(str))
            {
                return retval;
            }
           
            
            if (n > str.Length)
            {
                return retval;
            }
            n += 1;

            for (int i = 0; i != str.Length - (n - 2); ++i)
            {
                string tp = string.Empty;
                for (int j = 0; j != n-1; ++j)
                {
                    tp += char.ToString(str[i + j]);
                }
                retval.Add(tp);
            }
            return retval;
        }

        public static System.Collections.Generic.List<string> getNCorrelations(string str, int depth)
        {
            System.Collections.Generic.List<string> retval = new List<string>();
            if (string.IsNullOrEmpty(str))
            {
                return retval;
            }
            depth += 1;

            for (int i = 0; i != str.Length - (depth); ++i)
            {
                if (depth >= str.Length)
                { break; }
                int idx2 = i + depth;
                string tp = char.ToString(str[i]) + char.ToString(str[idx2]);
                retval.Add(tp);
            }
            return retval;
        }



    }
    
}
