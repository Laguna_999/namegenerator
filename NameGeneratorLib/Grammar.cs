﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NameGeneratorLib
{
    public class Grammar : INameGenerator
    {

        private System.Collections.Generic.Dictionary<string, float> charFrequencies;
        private System.Collections.Generic.Dictionary<string, float> Consonants;
        private System.Collections.Generic.Dictionary<string, float> Vowels;

        private List<string> endings;

        public Grammar (string collection = "grammar")
        {
            charFrequencies = new Dictionary<string, float>();
            Consonants = new Dictionary<string, float>();
            Vowels = new Dictionary<string, float>();
            endings = new List<string>();
            LoadCharFrequencies(collection);
            LoadEndings(collection);

        }

        private void LoadEndings(string collection)
        {
            string filename = "../../../data/" + collection + "/endings.json";

            if (!System.IO.File.Exists(filename))
            {
                throw new System.IO.IOException("no charfrequencies file found at " + filename);
            }

            string var = System.IO.File.ReadAllText(filename);
            endings = JsonConvert.DeserializeObject<List<string>>(var);
        }

        private void LoadCharFrequencies(string collection)
        {
            string filename = "../../../data/" + collection + "/charfrequency.json";

            if (!System.IO.File.Exists(filename))
            {
                throw new System.IO.IOException("no charfrequencies file found at " + filename);
            }

            string var = System.IO.File.ReadAllText(filename);
            charFrequencies = JsonConvert.DeserializeObject<Dictionary<string, float>>(var);

            foreach(var kvp in charFrequencies)
            {
                if (kvp.Key == "a" || kvp.Key == "e" || kvp.Key == "i" || kvp.Key == "o" || kvp.Key == "u")
                {
                    Vowels.Add(kvp.Key, kvp.Value);
                }
                else
                {
                    Consonants.Add(kvp.Key, kvp.Value);
                }
            }

            {
                float sum = 0;
                var tmp = new Dictionary<string, float>();
                foreach (var kvp in Vowels)
                {
                    sum += kvp.Value;
                    tmp.Add(kvp.Key, kvp.Value);
                }
                Vowels.Clear();
                foreach (var kvp in tmp)
                {
                    Vowels.Add(kvp.Key, kvp.Value / sum);
                }

            }
            {
                float sum = 0;
                var tmp = new Dictionary<string, float>();
                foreach (var kvp in Consonants)
                {
                    sum += kvp.Value;
                    tmp.Add(kvp.Key, kvp.Value);
                }
                Consonants.Clear();
                foreach(var kvp in tmp)
                {
                    Consonants.Add(kvp.Key, kvp.Value / sum);
                }
                
            }
        }

        private string getFromDict(Dictionary<string, float> d)
        {
            float rv = (float)(RandomLib.RandomGenerator.Random.NextDouble());
            float sum = 0;
            foreach (var kvp in d)
            {
                sum += kvp.Value;
                if (rv <= sum)
                {
                    return kvp.Key;
                }
            }

            return string.Empty;
        }

        private string getConsonant()
        {
            string s = string.Empty;
            s = getFromDict(Consonants);
            return s;
        }

        private string getVowel ()
        {
            string s = string.Empty;
            s = getFromDict(Vowels);
            return s;
        }

        private string getEnding()
        {
            string s = string.Empty;
            s = endings.ElementAt(RandomLib.RandomGenerator.Random.Next(endings.Count));
            return s;
        }


        public string getName(int l)
        {
            string retval = string.Empty;

            retval = getConsonant();
            retval += getVowel();
            retval += getConsonant();
            retval += getEnding();
            return retval;
        }


    }
}
