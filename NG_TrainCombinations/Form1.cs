﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NG_TrainCombinations
{
    public partial class Form1 : Form
    {
        NameGeneratorLib.PairsAndCorrelations NG;


        public Form1()
        {
            InitializeComponent();
            NG = new NameGeneratorLib.PairsAndCorrelations();

            updateCollectionsList();
        }

        private void updateCollectionsList()
        {
            var l = System.IO.Directory.EnumerateDirectories("../../../data/");
            foreach (var d in l)
            {
                string collection = d.Replace("../../../data/", "");
                cb_collection.Items.Add(collection);
            }
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            NewName();
        }

        private void NewName()
        {
            int l = RandomLib.RandomGenerator.Random.Next(3,10);
            tb_generatedName.Text = NG.PermuteName(NG.getRanomNameWithFrequencies(l));
            tb_value.Text = (NG.getValue(tb_generatedName.Text).ToString());
            tb_optimGain.Text = NG.lastOptimizationGain.ToString();
        }

        private void tb_generatedName_TextChanged(object sender, EventArgs e)
        {
            {
                var l = NameGeneratorLib.PairsAndCorrelations.getNPairs(tb_generatedName.Text, 2);
                string s = string.Empty;

                foreach (var kvp in l)
                {
                    s += kvp + " ";
                }
                tb_twopairs.Text = s;
            }
            {
                var l = NameGeneratorLib.PairsAndCorrelations.getNPairs(tb_generatedName.Text, 3);
                string s = string.Empty;

                foreach (var kvp in l)
                {
                    s += kvp + " ";
                }
                tb_threepairs.Text = s;
            }
            {
                var l = NameGeneratorLib.PairsAndCorrelations.getNPairs(tb_generatedName.Text, 4);
                string s = string.Empty;

                foreach (var kvp in l)
                {
                    s += kvp + " ";
                }
                tb_fourpairs.Text = s;
            }

            {
                var l = NameGeneratorLib.PairsAndCorrelations.getNCorrelations(tb_generatedName.Text, 1);
                string s = string.Empty;

                foreach (var kvp in l)
                {
                    s += kvp + " ";
                }
                tb_correl1.Text = s;
            }
            {
                var l = NameGeneratorLib.PairsAndCorrelations.getNCorrelations(tb_generatedName.Text, 2);
                string s = string.Empty;

                foreach (var kvp in l)
                {
                    s += kvp + " ";
                }
                tb_correl2.Text = s;
            }
            {
                var l = NameGeneratorLib.PairsAndCorrelations.getNCorrelations(tb_generatedName.Text, 3);
                string s = string.Empty;

                foreach (var kvp in l)
                {
                    s += kvp + " ";
                }
                tb_correl3.Text = s;
            }

        }

        private void btn_good_Click(object sender, EventArgs e)
        {
            Rate(1.0f);
            NewName();
        }
        private void btn_bad_Click(object sender, EventArgs e)
        {
            Rate(-3.0f);
            NewName();
        }

        private void Rate(float val)
        {
            NG.SetRating(tb_generatedName.Text, val);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NG.SaveFiles();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Rate(3.0f);
            NewName();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Rate(-6.0f);
            NewName();
        }

        private void cb_collection_SelectionChangeCommitted(object sender, EventArgs e)
        {
           

        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            string collection = cb_collection.SelectedItem.ToString();
            NG.LoadFiles(collection);
        }

        private void bt_Learn_Click(object sender, EventArgs e)
        {
            string collection = tb_NewName.Text;

            System.Windows.Forms.OpenFileDialog diag = new OpenFileDialog();
            if (diag.ShowDialog() == DialogResult.OK)
            {
                string trainingFile = diag.FileName;
                NG.LoadFiles(collection, trainingFile);
            }

            updateCollectionsList();

        }


        private void cb_tupels_CheckedChanged(object sender, EventArgs e)
        {
            NG.useTupels = cb_tupels.Checked;
        }

        private void o_CheckedChanged(object sender, EventArgs e)
        {
            NG.useCorrelations = cb_correlations.Checked;

        }
    }
}
