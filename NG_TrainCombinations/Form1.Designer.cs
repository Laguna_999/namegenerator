﻿namespace NG_TrainCombinations
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_generatedName = new System.Windows.Forms.TextBox();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_good = new System.Windows.Forms.Button();
            this.btn_bad = new System.Windows.Forms.Button();
            this.tb_twopairs = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_threepairs = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_fourpairs = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_value = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_correl3 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_correl2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_correl1 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tb_optimGain = new System.Windows.Forms.TextBox();
            this.cb_collection = new System.Windows.Forms.ComboBox();
            this.btn_Load = new System.Windows.Forms.Button();
            this.bt_Learn = new System.Windows.Forms.Button();
            this.tb_NewName = new System.Windows.Forms.TextBox();
            this.gb_tupels = new System.Windows.Forms.GroupBox();
            this.tb_correlations = new System.Windows.Forms.GroupBox();
            this.cb_tupels = new System.Windows.Forms.CheckBox();
            this.cb_correlations = new System.Windows.Forms.CheckBox();
            this.gb_tupels.SuspendLayout();
            this.tb_correlations.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_generatedName
            // 
            this.tb_generatedName.Location = new System.Drawing.Point(93, 38);
            this.tb_generatedName.Name = "tb_generatedName";
            this.tb_generatedName.ReadOnly = true;
            this.tb_generatedName.Size = new System.Drawing.Size(151, 20);
            this.tb_generatedName.TabIndex = 0;
            this.tb_generatedName.TextChanged += new System.EventHandler(this.tb_generatedName_TextChanged);
            // 
            // btn_next
            // 
            this.btn_next.Location = new System.Drawing.Point(12, 38);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(75, 23);
            this.btn_next.TabIndex = 1;
            this.btn_next.Text = "Create";
            this.btn_next.UseVisualStyleBackColor = true;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_good
            // 
            this.btn_good.Location = new System.Drawing.Point(174, 64);
            this.btn_good.Name = "btn_good";
            this.btn_good.Size = new System.Drawing.Size(75, 23);
            this.btn_good.TabIndex = 3;
            this.btn_good.Text = "Good";
            this.btn_good.UseVisualStyleBackColor = true;
            this.btn_good.Click += new System.EventHandler(this.btn_good_Click);
            // 
            // btn_bad
            // 
            this.btn_bad.Location = new System.Drawing.Point(255, 64);
            this.btn_bad.Name = "btn_bad";
            this.btn_bad.Size = new System.Drawing.Size(75, 23);
            this.btn_bad.TabIndex = 4;
            this.btn_bad.Text = "Bad";
            this.btn_bad.UseVisualStyleBackColor = true;
            this.btn_bad.Click += new System.EventHandler(this.btn_bad_Click);
            // 
            // tb_twopairs
            // 
            this.tb_twopairs.Location = new System.Drawing.Point(138, 20);
            this.tb_twopairs.Name = "tb_twopairs";
            this.tb_twopairs.ReadOnly = true;
            this.tb_twopairs.Size = new System.Drawing.Size(313, 20);
            this.tb_twopairs.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Two Pairs";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(57, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Three Pairs";
            // 
            // tb_threepairs
            // 
            this.tb_threepairs.Location = new System.Drawing.Point(138, 46);
            this.tb_threepairs.Name = "tb_threepairs";
            this.tb_threepairs.ReadOnly = true;
            this.tb_threepairs.Size = new System.Drawing.Size(313, 20);
            this.tb_threepairs.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Four Pairs";
            // 
            // tb_fourpairs
            // 
            this.tb_fourpairs.Location = new System.Drawing.Point(138, 72);
            this.tb_fourpairs.Name = "tb_fourpairs";
            this.tb_fourpairs.ReadOnly = true;
            this.tb_fourpairs.Size = new System.Drawing.Size(313, 20);
            this.tb_fourpairs.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(527, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 11;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_value
            // 
            this.tb_value.Location = new System.Drawing.Point(412, 38);
            this.tb_value.Name = "tb_value";
            this.tb_value.ReadOnly = true;
            this.tb_value.Size = new System.Drawing.Size(109, 20);
            this.tb_value.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "One Correl";
            // 
            // tb_correl3
            // 
            this.tb_correl3.Location = new System.Drawing.Point(138, 67);
            this.tb_correl3.Name = "tb_correl3";
            this.tb_correl3.ReadOnly = true;
            this.tb_correl3.Size = new System.Drawing.Size(313, 20);
            this.tb_correl3.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Three Correl";
            // 
            // tb_correl2
            // 
            this.tb_correl2.Location = new System.Drawing.Point(138, 41);
            this.tb_correl2.Name = "tb_correl2";
            this.tb_correl2.ReadOnly = true;
            this.tb_correl2.Size = new System.Drawing.Size(313, 20);
            this.tb_correl2.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Two Correl";
            // 
            // tb_correl1
            // 
            this.tb_correl1.Location = new System.Drawing.Point(138, 15);
            this.tb_correl1.Name = "tb_correl1";
            this.tb_correl1.ReadOnly = true;
            this.tb_correl1.Size = new System.Drawing.Size(313, 20);
            this.tb_correl1.TabIndex = 13;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(93, 64);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Very Good";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(336, 64);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 20;
            this.button3.Text = "Very Bad";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tb_optimGain
            // 
            this.tb_optimGain.Location = new System.Drawing.Point(412, 64);
            this.tb_optimGain.Name = "tb_optimGain";
            this.tb_optimGain.ReadOnly = true;
            this.tb_optimGain.Size = new System.Drawing.Size(109, 20);
            this.tb_optimGain.TabIndex = 21;
            // 
            // cb_collection
            // 
            this.cb_collection.FormattingEnabled = true;
            this.cb_collection.Location = new System.Drawing.Point(12, 11);
            this.cb_collection.Name = "cb_collection";
            this.cb_collection.Size = new System.Drawing.Size(232, 21);
            this.cb_collection.TabIndex = 22;
            this.cb_collection.SelectionChangeCommitted += new System.EventHandler(this.cb_collection_SelectionChangeCommitted);
            // 
            // btn_Load
            // 
            this.btn_Load.Location = new System.Drawing.Point(250, 9);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(75, 23);
            this.btn_Load.TabIndex = 23;
            this.btn_Load.Text = "Load";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // bt_Learn
            // 
            this.bt_Learn.Location = new System.Drawing.Point(527, 9);
            this.bt_Learn.Name = "bt_Learn";
            this.bt_Learn.Size = new System.Drawing.Size(75, 23);
            this.bt_Learn.TabIndex = 24;
            this.bt_Learn.Text = "Learn";
            this.bt_Learn.UseVisualStyleBackColor = true;
            this.bt_Learn.Click += new System.EventHandler(this.bt_Learn_Click);
            // 
            // tb_NewName
            // 
            this.tb_NewName.Location = new System.Drawing.Point(412, 11);
            this.tb_NewName.Name = "tb_NewName";
            this.tb_NewName.Size = new System.Drawing.Size(109, 20);
            this.tb_NewName.TabIndex = 25;
            // 
            // gb_tupels
            // 
            this.gb_tupels.Controls.Add(this.cb_tupels);
            this.gb_tupels.Controls.Add(this.label3);
            this.gb_tupels.Controls.Add(this.tb_fourpairs);
            this.gb_tupels.Controls.Add(this.label2);
            this.gb_tupels.Controls.Add(this.tb_threepairs);
            this.gb_tupels.Controls.Add(this.label1);
            this.gb_tupels.Controls.Add(this.tb_twopairs);
            this.gb_tupels.Location = new System.Drawing.Point(12, 93);
            this.gb_tupels.Name = "gb_tupels";
            this.gb_tupels.Size = new System.Drawing.Size(457, 108);
            this.gb_tupels.TabIndex = 26;
            this.gb_tupels.TabStop = false;
            this.gb_tupels.Text = "Tupels";
            // 
            // tb_correlations
            // 
            this.tb_correlations.Controls.Add(this.cb_correlations);
            this.tb_correlations.Controls.Add(this.label4);
            this.tb_correlations.Controls.Add(this.tb_correl3);
            this.tb_correlations.Controls.Add(this.label5);
            this.tb_correlations.Controls.Add(this.tb_correl2);
            this.tb_correlations.Controls.Add(this.label6);
            this.tb_correlations.Controls.Add(this.tb_correl1);
            this.tb_correlations.Location = new System.Drawing.Point(12, 207);
            this.tb_correlations.Name = "tb_correlations";
            this.tb_correlations.Size = new System.Drawing.Size(457, 99);
            this.tb_correlations.TabIndex = 27;
            this.tb_correlations.TabStop = false;
            this.tb_correlations.Text = "Correlations";
            // 
            // cb_tupels
            // 
            this.cb_tupels.AutoSize = true;
            this.cb_tupels.Checked = true;
            this.cb_tupels.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_tupels.Location = new System.Drawing.Point(15, 48);
            this.cb_tupels.Name = "cb_tupels";
            this.cb_tupels.Size = new System.Drawing.Size(15, 14);
            this.cb_tupels.TabIndex = 11;
            this.cb_tupels.UseVisualStyleBackColor = true;
            this.cb_tupels.CheckedChanged += new System.EventHandler(this.cb_tupels_CheckedChanged);
            // 
            // cb_correlations
            // 
            this.cb_correlations.AutoSize = true;
            this.cb_correlations.Checked = true;
            this.cb_correlations.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_correlations.Location = new System.Drawing.Point(15, 44);
            this.cb_correlations.Name = "cb_correlations";
            this.cb_correlations.Size = new System.Drawing.Size(15, 14);
            this.cb_correlations.TabIndex = 12;
            this.cb_correlations.UseVisualStyleBackColor = true;
            this.cb_correlations.CheckedChanged += new System.EventHandler(this.o_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 461);
            this.Controls.Add(this.tb_correlations);
            this.Controls.Add(this.gb_tupels);
            this.Controls.Add(this.tb_NewName);
            this.Controls.Add(this.bt_Learn);
            this.Controls.Add(this.btn_Load);
            this.Controls.Add(this.cb_collection);
            this.Controls.Add(this.tb_optimGain);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tb_value);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_bad);
            this.Controls.Add(this.btn_good);
            this.Controls.Add(this.btn_next);
            this.Controls.Add(this.tb_generatedName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gb_tupels.ResumeLayout(false);
            this.gb_tupels.PerformLayout();
            this.tb_correlations.ResumeLayout(false);
            this.tb_correlations.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_generatedName;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_good;
        private System.Windows.Forms.Button btn_bad;
        private System.Windows.Forms.TextBox tb_twopairs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_threepairs;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_fourpairs;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_value;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_correl3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_correl2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_correl1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox tb_optimGain;
        private System.Windows.Forms.ComboBox cb_collection;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.Button bt_Learn;
        private System.Windows.Forms.TextBox tb_NewName;
        private System.Windows.Forms.GroupBox gb_tupels;
        private System.Windows.Forms.CheckBox cb_tupels;
        private System.Windows.Forms.GroupBox tb_correlations;
        private System.Windows.Forms.CheckBox cb_correlations;
    }
}

