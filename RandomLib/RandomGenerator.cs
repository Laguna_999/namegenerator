﻿using System;


namespace RandomLib
{
    public static class RandomGenerator
    {
        static public Random Random { get { if (_random == null) { _random = new Random(); } return _random; } }
        static private Random _random;
        public static T GetRandomEnumValue<T>(T enumeration)
        {
            var values = Enum.GetValues(enumeration.GetType());

            return (T)values.GetValue(_random.Next(values.Length));
        }
    }
}
